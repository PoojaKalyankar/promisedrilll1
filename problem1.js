/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs').promises;
const path = require('node:path');

function operationOnfiles(numberOfFiles) {
    let pathForDirectory = '/home/pooja/promise/directory1'
    fs.mkdir(pathForDirectory)
        .then(function () {
            console.log("created the directory")
            for (let index = 0; index < numberOfFiles; index++) {
                let fileName = `randomFile${index + 1}.json`
                let filePath = path.join(pathForDirectory, fileName)
                console.log(`creating the file ${fileName}`)
                fs.writeFile(filePath, ("this is a JSON File"))
            }
        })
        .then(function () {
            for (let index = 0; index < numberOfFiles; index++) {
                let filename = `randomFile${index + 1}.json`;
                let filePath = path.join(pathForDirectory, filename);
                fs.unlink(filePath);
                console.log("deleting the file");
            }
        })
        .catch(function (error) {
            console.error(error);
        })
}


module.exports = operationOnfiles;
