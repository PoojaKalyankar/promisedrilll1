/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions,
     do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. 
        Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. 
        Then split the contents into sentences. 
        Then write it to a new file. 
        Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file.
         Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt 
        and delete all the new files that are mentioned 
        in that list simultaneously.
*/
const fs = require('fs').promises;

function operationsOnGivenFile() {
    fs.readFile('../lipsum.txt', 'utf8')
        .then(function (result) {
            console.log("getting the file content")
            return result.toUpperCase();
        })
        .then(function (result) {
            console.log("creating the new file with upperCase");
            fs.writeFile('./file1.txt', result);
            fs.appendFile('./filenames.txt', './file1.txt\n');
            console.log("adding file1.txt to filenames")
            return './file1.txt';
        })
        .then(function (result) {
            let upperCase = fs.readFile(result, 'utf8')
            console.log("reading the upperCase file")
            return upperCase;
        })
        .then(function (result) {
            console.log("converting the content to lowercase")
            return result.toLowerCase();
        })
        .then(function (result) {
            let string = result.split('.').join('\n')
            console.log("spliting the content in to lines");
            return string;
        })
        .then(function (result) {
            console.log("creating the file with new data")
            fs.writeFile('./file2.txt', result);
            return './file2.txt\n';
        })
        .then(function (result) {
            console.log("add the file2.txt in to filenames.txt");
            fs.appendFile('./filenames.txt', result)
            return './file2.txt';
        })
        .then(function (fileName) {
            console.log("reading the file2.txt")
            let content = fs.readFile(fileName, 'utf8')
            return content;
        })
        .then(function (fileContent) {
            console.log("sorting the content of the file")
            fileContent = fileContent.split(",").sort().toString();
            return fileContent;
        })
        .then(function (fileData) {
            console.log("creating file3.txt with sorted data")
            fs.writeFile('./file3.txt', fileData)
            return './file3.txt\n'
        })
        .then(function (fileName) {
            console.log("adding the file3.txt to filenames")
            fs.appendFile('./filenames.txt', fileName);
            return './filenames.txt';
        })
        .then(function (file) {
            console.log("reading the filenames.txt")
            let namesOfFile = fs.readFile(file, 'utf8')
            return namesOfFile;
        })
        .then(function (content) {
            let array = content.trim().split('\n');
            console.log("deleting the each file which are created");
            array.forEach(element => {
                fs.unlink(element);
            });
        })
        .catch(function (error) {
            console.error(error);
        })
}


module.exports = operationsOnGivenFile;
